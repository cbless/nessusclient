/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

/**
 * This class provides some basic methods for authentication, which are 
 * essential for all Manager classes. By delegating the authentication methods 
 * to the NessusClient it is possible to call this methods on all Manager #
 * classes.
 * 
 * @author Christoph Bless
 */
public class NessusManager {

    protected NessusClient client;

    public NessusManager(NessusClient client) {
        this.client = client;
    }
    
    /**
     * This method allows to authenticate to the server.
     * 
     * @param username The username to use.
     * @param password The password to use for the user.
     * @throws SessionException This exception is thrown if the username or 
     *      password was invalid or if too many users are connected.
     * @throws NessusException If any other error occurs.
     */
    public void login(String username, String password) 
            throws SessionException, NessusException {
        client.login(username, password);
    }

    /**
     * This methods checks if the user is authenticated.
     * @return <code>true</code> if the user is authenticated, 
     *      <code>false</code> otherwise.
     */
    public boolean isAuthenticated() {
        return client.isAuthenticated();
    }

    /**
     * This method is used to logout the user.
     * @throws SessionException This exception is thrown if the logout was not 
     *      successful
     * @throws NessusException  This exception is thrown if any other error 
     *      occurs.
     */
    public void logout() throws SessionException, NessusException {
        client.logout();
    }

}
