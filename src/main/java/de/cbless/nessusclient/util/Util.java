/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.util;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * @author Christoph Bless
 */
public class Util {
    
    public static void debugMarshall(Object obj){
        String result ="";
        
        try{
            JAXBContext context;
            StringWriter writer  = new StringWriter();
            context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(obj,writer);
            result = writer.toString();
            System.out.println(result);
        }catch (JAXBException ex){
            ex.printStackTrace();
        }
    }
}
