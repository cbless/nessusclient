/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.session.commands;

import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.NessusPostCommand;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author christoph
 */
public class LoginCommand extends NessusPostCommand {
    
    private String username;
    private String password;

    public LoginCommand() {
    }
    
    public LoginCommand(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
    
    @Override
    public Entity getEntity() {
        Form form = new Form();
        form.param("username", username);
        form.param("password", password);
        return Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
    }

    @Override
    public String getPath() {
        return NessusModule.Session.getPath();
    }
    
}
