
package de.cbless.nessusclient.session.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * On response code 200 the session token is set.
 * On error we get an response code 400, 401 or 500.
 * 
 * The response code 400 ist returned if the username format is not valid
 * The response code 401 ist returned if the username or password is invalid
 * The response code 400 ist returned if too many users are connected
 * 
 * @author christoph
 */
@XmlRootElement
public class LoginResponse {
    
    /**
     * Contains the session token if the response code was 200.
     */
    @XmlElement
    private String token;
    
    /**
     * contains an error message if the response code was 400, 401 or 500
     */
    @XmlElement
    private String error;

    public LoginResponse() {
    }

    
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    
}
