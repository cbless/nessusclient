/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.session;

import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.NessusManager;
import de.cbless.nessusclient.SessionException;
import de.cbless.nessusclient.session.commands.LoginCommand;
import de.cbless.nessusclient.session.commands.LogoutCommand;
import de.cbless.nessusclient.session.model.LoginResponse;
import javax.ws.rs.WebApplicationException;

/**
 * This class provides access to the commands of the session module of the 
 * Nessus server.
 * 
 * @author Christoph Bless
 */
public class SessionManager extends NessusManager {

    public SessionManager(NessusClient client) {
        super(client);
    }
    
    
    /**
     * Creates a new session token for the given user. 
     * 
     * @param username The username for the person who is attempting to log in.
     * @param password The password for the person who is attempting to log in.
     * @return The session token. 
     * @throws SessionException Throws a SessionException if the authetication 
     *  process failed. 
     * @throws NessusException Throws a NessusException if any other error 
     *  occurs.
     * 
     */
    public String doLogin(String username, String password) 
            throws NessusException, SessionException {
        LoginCommand cmd = new LoginCommand(username, password);
        try{
            LoginResponse response = client.post(cmd, LoginResponse.class);
            if (response.getToken() != null){
                return response.getToken();
            }else{
                throw new SessionException("Login failed");
            }
        } catch(WebApplicationException ex){
            int status = ex.getResponse().getStatus();
            switch(status){
                case 400: // Returned if the username format is not valid
                case 401: // Returned if the username or password is invalid
                case 500: // Returned if too many users are connected
                    // handle all three errors above
                    throw new SessionException(ex);
                default:
                    throw new NessusException(ex);
            }
        }

    }
     
    /**
     * Logs the current user out and destroys the session. 
     * @throws SessionException This exception is thrown if the logout was not 
     *      successful.
     * @throws NessusException  This exception is thrown if any other error 
     *      occurs.
     */
    public void doLogout() throws NessusException, SessionException {
        LogoutCommand cmd = new LogoutCommand();
        
        try{
            client.delete(cmd);
        } catch(WebApplicationException ex ){
            if (ex.getResponse().getStatus() != 200){
                throw new SessionException("Logout failed");
            }
        } catch(Exception ex){
            throw new NessusException(ex);
        }
    }
}
