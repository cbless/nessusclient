/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardOSCountHistory {
    
    @XmlElement 
    private int windows;
    
    @XmlElement 
    private int linux;
    
    @XmlElement 
    private int mac;
    
    @XmlElement 
    private int other;
    
    @XmlElement 
    private int total;
    
    @XmlElement(name="scan_id")
    private int scanId;
    
    @XmlElement(name="report_date")
    private int reportDate;

    public DashboardOSCountHistory() {
    }

    
    
    public int getWindows() {
        return windows;
    }

    public void setWindows(int windows) {
        this.windows = windows;
    }

    public int getLinux() {
        return linux;
    }

    public void setLinux(int linux) {
        this.linux = linux;
    }

    public int getMac() {
        return mac;
    }

    public void setMac(int mac) {
        this.mac = mac;
    }

    public int getOther() {
        return other;
    }

    public void setOther(int other) {
        this.other = other;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getScanId() {
        return scanId;
    }

    public void setScanId(int scanId) {
        this.scanId = scanId;
    }

    public int getReportDate() {
        return reportDate;
    }

    public void setReportDate(int reportDate) {
        this.reportDate = reportDate;
    }

    @Override
    public String toString() {
        return "DashboardOSCountHistory{" + "windows=" + windows + ", linux=" + linux + ", mac=" + mac + ", other=" + other + ", total=" + total + ", scanId=" + scanId + ", reportDate=" + reportDate + '}';
    }
    
    
    
}
