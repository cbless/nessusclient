/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Setting {
    
    @XmlElement
    private String name;
    
    @XmlElement
    private String description;
    
    @XmlElement
    private String emails;
    
    @XmlElement
    private Boolean enabled;
    
    @XmlElement
    private String launch;
    
    @XmlElement(name="folder_id")
    private int folderId;
    
    @XmlElement(name="policy_id")
    private int policyId;
    
    @XmlElement(name = "scanner_id")
    private int scannerId;
    
    @XmlElement(name = "text_targets")
    private String textTargets;
    
    @XmlElement(name = "use_dashboard")
    private Boolean useDashboard;

    @XmlElement
    private String starttime;
    
    @XmlElement
    private String rrules;
    
    @XmlElement
    private String timezone;
    
    @XmlElement(name= "file_targets")
    private String fileTargets;
    
    //private String[] acls;
    
    
    
    public Setting() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getLaunch() {
        return launch;
    }

    public void setLaunch(String launch) {
        this.launch = launch;
    }

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getScannerId() {
        return scannerId;
    }

    public void setScannerId(int scannerId) {
        this.scannerId = scannerId;
    }

    public String getTextTargets() {
        return textTargets;
    }

    public void setTextTargets(String textTargets) {
        this.textTargets = textTargets;
    }

    public Boolean getUseDashboard() {
        return useDashboard;
    }

    public void setUseDashboard(Boolean useDashboard) {
        this.useDashboard = useDashboard;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getRrules() {
        return rrules;
    }

    public void setRrules(String rrules) {
        this.rrules = rrules;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getFileTargets() {
        return fileTargets;
    }

    public void setFileTargets(String fileTargets) {
        this.fileTargets = fileTargets;
    }

    @Override
    public String toString() {
        return "Setting{" + "name=" + name + ", description=" + description + ", emails=" + emails + ", enabled=" + enabled + ", launch=" + launch + ", folderId=" + folderId + ", policyId=" + policyId + ", scannerId=" + scannerId + ", textTargets=" + textTargets + ", useDashboard=" + useDashboard + ", starttime=" + starttime + ", rrules=" + rrules + ", timezone=" + timezone + ", fileTargets=" + fileTargets + '}';
    }

    
    
}
