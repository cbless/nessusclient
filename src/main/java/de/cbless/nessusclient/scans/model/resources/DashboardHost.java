/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The details of a host for the dashboard.
 * 
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardHost {
   
    /** 
     * The unique id of the host.
     */
    @XmlElement(name="host_id")
    private int hostId;
    
    /**
     * The index for the host.
     */
    @XmlElement(name="host_index")
    private String hostIndex;
    
    /**
     * The name of the host.
     */
    @XmlElement 
    private String hostname;
    
    /**
     * The overall severity rating of the host.
     */
    @XmlElement 
    private int severity;

    public DashboardHost() {
    }

    
    public int getHostId() {
        return hostId;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }

    public String getHostIndex() {
        return hostIndex;
    }

    public void setHostIndex(String hostIndex) {
        this.hostIndex = hostIndex;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return "DashboardHost{" + "hostId=" + hostId + ", hostIndex=" + hostIndex + ", hostname=" + hostname + ", severity=" + severity + '}';
    }
    
    
    
}
