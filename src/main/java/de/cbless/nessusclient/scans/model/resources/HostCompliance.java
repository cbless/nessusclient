/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement(name="host_compliance")
@XmlAccessorType(XmlAccessType.FIELD)
public class HostCompliance {
    
    @XmlElement(name="host_id")
    private int hostid;
    
    @XmlElement
    private String hostname;
    
    @XmlElement(name="plugin_id")
    private int pluginId;
    
    @XmlElement(name="plugin_name")
    private String pluginName;
    
    @XmlElement(name="plugin_family")
    private String plugin_family;
    
    @XmlElement
    private int count;
    
    @XmlElement(name="sevirity_index")
    private int severityIndex;
    
    @XmlElement
    private int severity;

    public HostCompliance() {
    }

    
    public int getHostid() {
        return hostid;
    }

    public void setHostid(int hostid) {
        this.hostid = hostid;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPluginId() {
        return pluginId;
    }

    public void setPluginId(int pluginId) {
        this.pluginId = pluginId;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public String getPlugin_family() {
        return plugin_family;
    }

    public void setPlugin_family(String plugin_family) {
        this.plugin_family = plugin_family;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSeverityIndex() {
        return severityIndex;
    }

    public void setSeverityIndex(int severityIndex) {
        this.severityIndex = severityIndex;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }
    
    
    
}
