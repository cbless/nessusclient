/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Filter {
    @XmlElement
    private String name;
    
    @XmlElement(name="readable_name")
    private String readableName;
    
    //TODO:
    @XmlElement
    private Object[] operators;
    
    @XmlElement
    private Control control;

    public Filter() {
    }

    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReadableName() {
        return readableName;
    }

    public void setReadableName(String readableName) {
        this.readableName = readableName;
    }

    public Object[] getOperators() {
        return operators;
    }

    public void setOperators(Object[] operators) {
        this.operators = operators;
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        this.control = control;
    }

    @Override
    public String toString() {
        return "Filter{" + "name=" + name + ", readableName=" + readableName + ", operators=" + operators + ", control=" + control + '}';
    }
    
    
}
