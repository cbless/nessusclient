/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Info {
 
    @XmlElement(name="edit_allowed")
    private Boolean editAllowed;
    
    @XmlElement
    private String status;
    
    @XmlElement
    private String policy;
    
    @XmlElement(name="pci-can-upload")
    private Boolean pciCanUpload;
    
    @XmlElement
    private Boolean hasaudittrail;
    
    @XmlElement(name="scan_start")
    private String scanStart;
    
    @XmlElement(name="folder_id")
    private Integer folderId;
    
    @XmlElement
    private String targets;
    
    @XmlElement
    private Integer timestamp;
    
    @XmlElement(name="object_id")
    private Integer objectId;
    
    @XmlElement(name="scanner_name")
    private String scannerName;
    
    @XmlElement
    private Boolean haskb;
    
    @XmlElement
    private String uuid;
    
    @XmlElement
    private Integer hostcount;
    
    @XmlElement(name="scan_end")
    private String scanEnd;
    
    @XmlElement
    private String name;
    
    @XmlElement(name="user_permissions")
    private Integer userPermissions;
    
    @XmlElement
    private Boolean control;

    public Info() {
    }

    
    public Boolean getEditAllowed() {
        return editAllowed;
    }

    public void setEditAllowed(Boolean editAllowed) {
        this.editAllowed = editAllowed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public Boolean getPciCanUpload() {
        return pciCanUpload;
    }

    public void setPciCanUpload(Boolean pciCanUpload) {
        this.pciCanUpload = pciCanUpload;
    }

    public Boolean getHasaudittrail() {
        return hasaudittrail;
    }

    public void setHasaudittrail(Boolean hasaudittrail) {
        this.hasaudittrail = hasaudittrail;
    }

    public String getScanStart() {
        return scanStart;
    }

    public void setScanStart(String ScanStart) {
        this.scanStart = ScanStart;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getScannerName() {
        return scannerName;
    }

    public void setScannerName(String scannerName) {
        this.scannerName = scannerName;
    }

    public Boolean getHaskb() {
        return haskb;
    }

    public void setHaskb(Boolean haskb) {
        this.haskb = haskb;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getHostcount() {
        return hostcount;
    }

    public void setHostcount(Integer hostcount) {
        this.hostcount = hostcount;
    }

    public String getScanEnd() {
        return scanEnd;
    }

    public void setScanEnd(String scanEnd) {
        this.scanEnd = scanEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(Integer userPermissions) {
        this.userPermissions = userPermissions;
    }

    public Boolean getControl() {
        return control;
    }

    public void setControl(Boolean control) {
        this.control = control;
    }

    @Override
    public String toString() {
        return "ScanInfo{" + "editAllowed=" + editAllowed + ", status=" + status + ", policy=" + policy + ", pciCanUpload=" + pciCanUpload + ", hasaudittrail=" + hasaudittrail + ", ScanStart=" + scanStart + ", folderId=" + folderId + ", targets=" + targets + ", timestamp=" + timestamp + ", objectId=" + objectId + ", scannerName=" + scannerName + ", haskb=" + haskb + ", uuid=" + uuid + ", hostcount=" + hostcount + ", scanEnd=" + scanEnd + ", name=" + name + ", userPermissions=" + userPermissions + ", control=" + control + '}';
    }
    
    
    
    
}
