/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;

import de.cbless.nessusclient.scans.model.resources.Dashboard;
import de.cbless.nessusclient.scans.model.resources.Filter;
import de.cbless.nessusclient.scans.model.resources.History;
import de.cbless.nessusclient.scans.model.resources.Host;
import de.cbless.nessusclient.scans.model.resources.Info;
import de.cbless.nessusclient.scans.model.resources.Note;
import de.cbless.nessusclient.scans.model.resources.Remediations;
import de.cbless.nessusclient.scans.model.resources.Vulnerability;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScanDetailsResponse {
    
    @XmlElement
    private Info info;

    @XmlElement
    private List<Host> hosts;
    
    @XmlElement
    private List<Host> comphosts;
    
    @XmlElement
    private List<Note> notes;

    @XmlElement
    private Remediations remediations;
    
    @XmlElement
    private List<Vulnerability> vulnerabilities;
    
    @XmlElement
    private List<Vulnerability> compliance;
    
    @XmlElement
    private List<History> history;
    
    @XmlElement
    private List<Filter> filters;
    
    @XmlElement
    private Dashboard dashboard;
    
    
    public ScanDetailsResponse() {
        hosts = new ArrayList<Host>();
        comphosts = new ArrayList<Host>();
        notes = new ArrayList<Note>();
        vulnerabilities = new ArrayList<Vulnerability>();
        compliance = new ArrayList<Vulnerability>();
        history = new ArrayList<History>();
        filters = new ArrayList<Filter>();
    }
            
    
    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Host> getHosts() {
        return hosts;
    }

    public void setHosts(List<Host> hosts) {
        this.hosts = hosts;
    }

    public List<Host> getComphosts() {
        return comphosts;
    }

    public void setComphosts(List<Host> comphosts) {
        this.comphosts = comphosts;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Remediations getRemediations() {
        return remediations;
    }

    public void setRemediations(Remediations remediations) {
        this.remediations = remediations;
    }

    public List<Vulnerability> getVulnerabilities() {
        return vulnerabilities;
    }

    public void setVulnerabilities(List<Vulnerability> vulnerabilities) {
        this.vulnerabilities = vulnerabilities;
    }

    public List<Vulnerability> getCompliance() {
        return compliance;
    }

    public void setCompliance(List<Vulnerability> compliance) {
        this.compliance = compliance;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    @Override
    public String toString() {
        return "ScanDetailsResponse{" + "info=" + info + ", hosts=" + hosts + ", comphosts=" + comphosts + ", notes=" + notes + ", remediations=" + remediations + ", vulnerabilities=" + vulnerabilities + ", compliance=" + compliance + ", history=" + history + ", filters=" + filters + ", dashboard=" + dashboard + '}';
    }
    
}
