/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListScansRequest {
    
    @XmlElement(name="folder_id")
    private int folderId;
    
    @XmlElement(name="last_modification_date")
    private int lastModificationDate;

    public ListScansRequest() {
    }

    
    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(int lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    
    
}
