/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Object containing the counts of the hosts scanned, if they were scanned with
 * or without authentication, and if they are newly scanned.
 * 
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardHostCount {
    
    /**
     * The number of hosts scanned with authentication.
     */
    @XmlElement 
    private int auth;
    
    /** 
     * The number of hosts scanned without authentication.
     */
    @XmlElement(name="no_auth")
    private int noAuth;
    
    /**
     * The number of hosts new to this scan and scanned with authentication.
     */
    @XmlElement(name="new_auth")
    private int newAuth;
    
    /** 
     * The number of hosts new to this scan and scanned without authentication.
     */
    @XmlElement(name="new_no_auth")
    private int newNoAuth;

    public DashboardHostCount() {
    }

    
    public int getAuth() {
        return auth;
    }

    public void setAuth(int auth) {
        this.auth = auth;
    }

    public int getNoAuth() {
        return noAuth;
    }

    public void setNoAuth(int noAuth) {
        this.noAuth = noAuth;
    }

    public int getNewAuth() {
        return newAuth;
    }

    public void setNewAuth(int newAuth) {
        this.newAuth = newAuth;
    }

    public int getNewNoAuth() {
        return newNoAuth;
    }

    public void setNewNoAuth(int newNoAuth) {
        this.newNoAuth = newNoAuth;
    }
    
    
}
