/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.model;

import de.cbless.nessusclient.scans.model.resources.HostCompliance;
import de.cbless.nessusclient.scans.model.resources.HostInfo;
import de.cbless.nessusclient.scans.model.resources.HostVulnerabilty;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class HostDetailsResponse {
    
    @XmlElement
    private HostInfo info;
    
    @XmlElement
    private List<HostCompliance> compliance;
    
    @XmlElement
    private List<HostVulnerabilty> vulnerabilities;

    public HostDetailsResponse() {
    }

    public HostInfo getInfo() {
        return info;
    }

    public void setInfo(HostInfo info) {
        this.info = info;
    }

    public List<HostCompliance> getCompliance() {
        return compliance;
    }

    public void setCompliance(List<HostCompliance> compliance) {
        this.compliance = compliance;
    }

    public List<HostVulnerabilty> getVulnerabilities() {
        return vulnerabilities;
    }

    public void setVulnerabilities(List<HostVulnerabilty> vulnerabilities) {
        this.vulnerabilities = vulnerabilities;
    }

    @Override
    public String toString() {
        return "HostDetailsResponse{" + "info=" + info + ", compliance=" + compliance + ", vulnerabilities=" + vulnerabilities + '}';
    }
    
    
}
