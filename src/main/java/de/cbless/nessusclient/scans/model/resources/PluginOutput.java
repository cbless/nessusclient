/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement(name ="plugin_output")
public class PluginOutput {
    
    @XmlElement(name="plugin_output")
    private String pluginOutput;
    
    private String hosts;
    
    private int severity;
    
    private Object ports;

    public PluginOutput() {
    }

    
    public String getPluginOutput() {
        return pluginOutput;
    }

    public void setPluginOutput(String pluginOutput) {
        this.pluginOutput = pluginOutput;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public Object getPorts() {
        return ports;
    }

    public void setPorts(Object ports) {
        this.ports = ports;
    }
    
    
}
