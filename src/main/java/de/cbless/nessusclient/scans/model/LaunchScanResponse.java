/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LaunchScanResponse {
    
    @XmlElement(name="scan_uuid")
    private String scanUUID;

    public LaunchScanResponse() {
    }

    public void setScanUUID(String scanUUID) {
        this.scanUUID = scanUUID;
    }

    public String getScanUUID() {
        return scanUUID;
    }

    @Override
    public String toString() {
        return "LaunchScanResponse{" + "scanUUID=" + scanUUID + '}';
    }
    
    
}
