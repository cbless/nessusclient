/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class HostInfo {
    
    @XmlAttribute(name="host_start")
    private String hostStart;
    
    @XmlAttribute(name="mac_address")
    private String macAddress;
    
    @XmlAttribute(name="host_fqdn")
    private String fqdn;
    
    @XmlAttribute(name="host_end")
    private String hostEnd;
    
    @XmlAttribute(name="operating-system")
    private String os;
    
    @XmlAttribute(name="host-ip")
    private String ip;

    public HostInfo() {
    }

    public String getHostStart() {
        return hostStart;
    }

    public void setHostStart(String hostStart) {
        this.hostStart = hostStart;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getHostEnd() {
        return hostEnd;
    }

    public void setHostEnd(String hostEnd) {
        this.hostEnd = hostEnd;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "HostInfo{" + "hostStart=" + hostStart + ", macAddress=" + macAddress + ", fqdn=" + fqdn + ", hostEnd=" + hostEnd + ", os=" + os + ", ip=" + ip + '}';
    }
    
    
}
