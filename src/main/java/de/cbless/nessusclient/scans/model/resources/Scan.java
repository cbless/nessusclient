/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Scan {
    
    /** The unique id of the scan */
    @XmlElement
    private int id;
    
    /** The uuid for the scan. */
    @XmlElement
    private String uuid;
    
    /** The name of the scan. */
    @XmlElement
    private String name;
    
    /** The owner of the scan. */
    @XmlElement
    private String owner;
    
    /** If true, the schedule for the scan is enabled. */
    @XmlElement
    private boolean enabled;
    
    /** The unique id of the folder housing the scan (only returned when 
     * folder_id is not in the list request). */
    @XmlElement(name = "folder_id")
    private int folderId;
    
    /** If true, the scan has been read. */
    @XmlElement
    private boolean read;
    
    /** The status of the scan (completed, aborted, imported, cancelled, 
     * running, paused, etc).*/
    @XmlElement
    private String status;
    
    /** If true, the scan is shared. */
    @XmlElement
    private boolean shared;
    
    /** The sharing permissions for the scan. */
    @XmlElement(name="user_permissions")
    private int userPermissons;
    
    /** The creation date for the scan in unixtime. */
    @XmlElement(name="creation_date")
    private int creationDate;
    
    /** The last modification date for the scan in unixtime. */
    @XmlElement(name="last_modifikation_date")
    private int lastModificationDate;
    
    /** If true, the scan has a schedule and can be launched. */
    @XmlElement
    private boolean control;
    
    /** The scheduled start time for the scan. */
    @XmlElement
    private String starttime;
    
    /** The timezone for the scan. */
    @XmlElement
    private String timezone;
    
    /** The timezone for the scan. */
    @XmlElement
    private String rrules;
    
    /** If true, the dashboard is enabled for the scan. */
    @XmlElement(name="use_dashboard")
    private boolean useDashboard;

    public Scan() {
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public int getUserPermissons() {
        return userPermissons;
    }

    public void setUserPermissons(int userPermissons) {
        this.userPermissons = userPermissons;
    }

    public int getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(int creationDate) {
        this.creationDate = creationDate;
    }

    public int getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(int lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public boolean isControl() {
        return control;
    }

    public void setControl(boolean control) {
        this.control = control;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getRrules() {
        return rrules;
    }

    public void setRrules(String rrules) {
        this.rrules = rrules;
    }

    public boolean isUseDashboard() {
        return useDashboard;
    }

    public void setUseDashboard(boolean useDashboard) {
        this.useDashboard = useDashboard;
    }

    
    
}
