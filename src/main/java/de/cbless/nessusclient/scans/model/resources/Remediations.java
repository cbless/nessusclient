/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author christoph
 */

public class Remediations {
    
    @XmlElement
    private List<Remediation> remediations;
    
    @XmlElement(name= "num_hosts")
    private int numHosts;
    
    @XmlElement(name= "num_cves")
    private int numCVEs;
    
    @XmlElement(name="num_impacted_hosts")
    private int numImpactedHosts;
    
    @XmlElement(name="num_remediated_cves")
    private int numRemediatedCVEs;

    public Remediations() {
        this.remediations = new ArrayList<Remediation>();
    }

    
    public List<Remediation> getRemediations() {
        return remediations;
    }

    public void setRemediations(List<Remediation> remediations) {
        this.remediations = remediations;
    }

    public int getNumHosts() {
        return numHosts;
    }

    public void setNumHosts(int numHosts) {
        this.numHosts = numHosts;
    }

    public int getNumCVEs() {
        return numCVEs;
    }

    public void setNumCVEs(int numCVEs) {
        this.numCVEs = numCVEs;
    }

    public int getNumImpactedHosts() {
        return numImpactedHosts;
    }

    public void setNumImpactedHosts(int numImpactedHosts) {
        this.numImpactedHosts = numImpactedHosts;
    }

    public int getNumRemediatedCVEs() {
        return numRemediatedCVEs;
    }

    public void setNumRemediatedCVEs(int numRemediatedCVEs) {
        this.numRemediatedCVEs = numRemediatedCVEs;
    }

    @Override
    public String toString() {
        return "Remediations{" + "remediations=" + remediations + ", numHosts=" + numHosts + ", numCVEs=" + numCVEs + ", numImpactedHosts=" + numImpactedHosts + ", numRemediatedCVEs=" + numRemediatedCVEs + '}';
    }
    
    
   
}
