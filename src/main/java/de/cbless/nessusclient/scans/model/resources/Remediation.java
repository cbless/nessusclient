/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Remediation {
    
    @XmlElement
    private String value;
    
    @XmlElement
    private String remediation;
    
    @XmlElement
    private int hosts;
    
    @XmlElement
    private int vulns;

    public Remediation() {
    }

    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemediation() {
        return remediation;
    }

    public void setRemediation(String remediation) {
        this.remediation = remediation;
    }

    public int getHosts() {
        return hosts;
    }

    public void setHosts(int hosts) {
        this.hosts = hosts;
    }

    public int getVulns() {
        return vulns;
    }

    public void setVulns(int vulns) {
        this.vulns = vulns;
    }

    @Override
    public String toString() {
        return "Remediation{" + "value=" + value + ", remediation=" + remediation + ", hosts=" + hosts + ", vulns=" + vulns + '}';
    }
    
    
}
