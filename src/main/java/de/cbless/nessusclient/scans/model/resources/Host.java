/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Host {
    
    @XmlElement(name= "host_id")
    private int hostID;
    
    @XmlElement(name= "host_index")
    private String hostIndex;
    
    @XmlElement
    private String hostname;
 
    @XmlElement
    private String progress;
    
    @XmlElement
    private int critical;
    
    @XmlElement
    private int high;
    
    @XmlElement
    private int medium;
    
    @XmlElement
    private int low;
    
    @XmlElement
    private int info;
    
    @XmlElement
    private int totalchecksconsidered;
    
    @XmlElement
    private int numchecksconsidered;
    
    @XmlElement
    private int scanprogresstotal;
    
    @XmlElement
    private int scanprogresscurrent;
    
    @XmlElement
    private int score;

    public Host() {
    }

    
    public int getHostID() {
        return hostID;
    }

    public void setHostID(int hostID) {
        this.hostID = hostID;
    }

    public String getHostIndex() {
        return hostIndex;
    }

    public void setHostIndex(String hostIndex) {
        this.hostIndex = hostIndex;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getCritical() {
        return critical;
    }

    public void setCritical(int critical) {
        this.critical = critical;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public int getMedium() {
        return medium;
    }

    public void setMedium(int medium) {
        this.medium = medium;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public int getTotalchecksconsidered() {
        return totalchecksconsidered;
    }

    public void setTotalchecksconsidered(int totalchecksconsidered) {
        this.totalchecksconsidered = totalchecksconsidered;
    }

    public int getNumchecksconsidered() {
        return numchecksconsidered;
    }

    public void setNumchecksconsidered(int numchecksconsidered) {
        this.numchecksconsidered = numchecksconsidered;
    }

    public int getScanprogresstotal() {
        return scanprogresstotal;
    }

    public void setScanprogresstotal(int scanprogresstotal) {
        this.scanprogresstotal = scanprogresstotal;
    }

    public int getScanprogresscurrent() {
        return scanprogresscurrent;
    }

    public void setScanprogresscurrent(int scanprogresscurrent) {
        this.scanprogresscurrent = scanprogresscurrent;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Host{" + "hostID=" + hostID + ", hostIndex=" + hostIndex + ", hostname=" + hostname + ", progress=" + progress + ", critical=" + critical + ", high=" + high + ", medium=" + medium + ", low=" + low + ", info=" + info + ", totalchecksconsidered=" + totalchecksconsidered + ", numchecksconsidered=" + numchecksconsidered + ", scanprogresstotal=" + scanprogresstotal + ", scanprogresscurrent=" + scanprogresscurrent + ", score=" + score + '}';
    }
    
    
}
