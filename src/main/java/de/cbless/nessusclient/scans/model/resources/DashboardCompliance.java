/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.model.resources;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardCompliance {
    
    private ScanComplianceCounts current;
    
    private List<ScanComplianceCountsHistory> history;

    public DashboardCompliance() {
        history = new ArrayList<ScanComplianceCountsHistory>();
    }

    public ScanComplianceCounts getCurrent() {
        return current;
    }

    public void setCurrent(ScanComplianceCounts current) {
        this.current = current;
    }

    public List<ScanComplianceCountsHistory> getHistory() {
        return history;
    }

    public void setHistory(List<ScanComplianceCountsHistory> history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "DashboardCompliance{" + "current=" + current + ", history=" + history + '}';
    }
    
    
}
