/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardHosts {
    
    @XmlElement
    private List<DashboardHost> top;
        
    @XmlElement
    private DashboardHostCount current;
    
    @XmlElement
    private DashboardOSCount os;
    
    @XmlElement
    private List<DashboardOSCountHistory> history;
    
    @XmlElement
    private List<DashboardHost> comp;

    public DashboardHosts() {
        top = new ArrayList<DashboardHost>();
        history = new ArrayList<DashboardOSCountHistory>();
        comp = new ArrayList<DashboardHost>();
    }

    public List<DashboardHost> getTop() {
        return top;
    }

    public void setTop(List<DashboardHost> top) {
        this.top = top;
    }

    public DashboardHostCount getCurrent() {
        return current;
    }

    public void setCurrent(DashboardHostCount current) {
        this.current = current;
    }

    public DashboardOSCount getOs() {
        return os;
    }

    public void setOs(DashboardOSCount os) {
        this.os = os;
    }

    public List<DashboardOSCountHistory> getHistory() {
        return history;
    }

    public void setHistory(List<DashboardOSCountHistory> history) {
        this.history = history;
    }

    public List<DashboardHost> getComp() {
        return comp;
    }

    public void setComp(List<DashboardHost> comp) {
        this.comp = comp;
    }

    @Override
    public String toString() {
        return "DashboardHosts{" + "top=" + top + ", current=" + current + ", os=" + os + ", history=" + history + ", comp=" + comp + '}';
    }
    
    
}
