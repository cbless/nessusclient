/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;

import de.cbless.nessusclient.scans.model.resources.Scan;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateScanResponse {
    
    @XmlElement
    private Scan scan;   

    public CreateScanResponse() {
    }

    public Scan getScan() {
        return scan;
    }

    public void setScan(Scan scan) {
        this.scan = scan;
    }

    @Override
    public String toString() {
        return "CreateScanResponse{" + "scan=" + scan + '}';
    }
    
    
     
    
}
