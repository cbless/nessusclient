/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DashboardVulnerbilities {
    
    @XmlElement
    private List<Vulnerability> top;
    
    @XmlElement
    private ScanVulnerabilityCounts current;
    
    @XmlElement
    private ScanVulnerabilityCountsHistory history;

    public DashboardVulnerbilities() {
        top = new ArrayList<Vulnerability>();
    }

    public List<Vulnerability> getTop() {
        return top;
    }

    public void setTop(List<Vulnerability> top) {
        this.top = top;
    }

    public ScanVulnerabilityCounts getCurrent() {
        return current;
    }

    public void setCurrent(ScanVulnerabilityCounts current) {
        this.current = current;
    }

    public ScanVulnerabilityCountsHistory getHistory() {
        return history;
    }

    public void setHistory(ScanVulnerabilityCountsHistory history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "Vulnerbilities{" + "top=" + top + ", current=" + current + ", history=" + history + '}';
    }
    
    
}
