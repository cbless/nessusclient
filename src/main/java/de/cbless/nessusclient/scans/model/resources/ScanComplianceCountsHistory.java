/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScanComplianceCountsHistory {
    
   @XmlElement 
   private int total;
   
   @XmlElement 
   private int pass;
   
   @XmlElement 
   private int warn;
   
   @XmlElement 
   private int fail;
   
   @XmlElement(name="report_date")
   private int reportDate;

    public ScanComplianceCountsHistory() {
    }

   
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPass() {
        return pass;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public int getWarn() {
        return warn;
    }

    public void setWarn(int warn) {
        this.warn = warn;
    }

    public int getFail() {
        return fail;
    }

    public void setFail(int fail) {
        this.fail = fail;
    }

    public int getReportDate() {
        return reportDate;
    }

    public void setReportDate(int reportDate) {
        this.reportDate = reportDate;
    }

    @Override
    public String toString() {
        return "ScanComplianceCountsHistory{" + "total=" + total + ", pass=" + pass + ", warn=" + warn + ", fail=" + fail + ", reportDate=" + reportDate + '}';
    }
   
   
}
