/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement(name = "host_vulnerability")
public class HostVulnerabilty {
    
    @XmlElement(name="host_id")
    private int hostId;
    
    private String hostname;
    
    @XmlElement(name="plugin_id")
    private int pluginId;
    
    @XmlElement(name="plugin_name")
    private String pluginName;
    
    @XmlElement(name="plugin_family")
    private String pluginFamily;
    
    private int count;
    
    @XmlElement(name="vuln_id")
    private int vulnIndex;
    
    @XmlElement(name="severity_index")
    private int severityIndex;
    
    @XmlElement
    private int severity;

    public HostVulnerabilty() {
    }

    public int getHostId() {
        return hostId;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPluginId() {
        return pluginId;
    }

    public void setPluginId(int pluginId) {
        this.pluginId = pluginId;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public String getPluginFamily() {
        return pluginFamily;
    }

    public void setPluginFamily(String pluginFamily) {
        this.pluginFamily = pluginFamily;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getVulnIndex() {
        return vulnIndex;
    }

    public void setVulnIndex(int vulnIndex) {
        this.vulnIndex = vulnIndex;
    }

    public int getSeverityIndex() {
        return severityIndex;
    }

    public void setSeverityIndex(int severityIndex) {
        this.severityIndex = severityIndex;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return "HostVulnerabilty{" + "hostId=" + hostId + ", hostname=" + hostname + ", pluginId=" + pluginId + ", pluginName=" + pluginName + ", pluginFamily=" + pluginFamily + ", count=" + count + ", vulnIndex=" + vulnIndex + ", severityIndex=" + severityIndex + ", severity=" + severity + '}';
    }
    
    
}
