/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * https://localhost:8834/nessus6-api.html#/resources/scans
 * 
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class History {

    @XmlElement(name="history_id")
    private int historyId;
    
    @XmlElement
    private String uuid;
    
    @XmlElement(name="owner_id")
    private int ownerID;
    
    @XmlElement
    private String status;
    
    @XmlElement(name="creation_date")
    private int creation_date;
    
    @XmlElement(name= "last_modification_date")
    private int lastModificationDate;

    public History() {
    }

    
    public int getHistoryId() {
        return historyId;
    }

    public void setHistoryId(int historyId) {
        this.historyId = historyId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(int creation_date) {
        this.creation_date = creation_date;
    }

    public int getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(int lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    @Override
    public String toString() {
        return "History{" + "historyId=" + historyId + ", uuid=" + uuid + ", ownerID=" + ownerID + ", status=" + status + ", creation_date=" + creation_date + ", lastModificationDate=" + lastModificationDate + '}';
    }
    
    
}
