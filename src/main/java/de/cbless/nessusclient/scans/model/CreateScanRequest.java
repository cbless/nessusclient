/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;

import de.cbless.nessusclient.scans.model.resources.Setting;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateScanRequest {
    
    @XmlElement(name="uuid")
    private String templateUUID;
    
    @XmlElement
    private Setting settings;

    public CreateScanRequest() {
    }

    
    public String getTemplateUUID() {
        return templateUUID;
    }

    public void setTemplateUUID(String templateUUID) {
        this.templateUUID = templateUUID;
    }

    public Setting getSettings() {
        return settings;
    }

    public void setSettings(Setting settings) {
        this.settings = settings;
    }

    @Override
    public String toString() {
        return "CreateScanRequest{" + "templateUUID=" + templateUUID + ", settings=" + settings + '}';
    }
    
    
    
}
