/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.model;


import de.cbless.nessusclient.folders.model.resources.Folder;
import de.cbless.nessusclient.scans.model.resources.Scan;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListScansResponse {
    
    @XmlElement(name="folders")
    private List<Folder> folders;
    
    @XmlElement(name="scans")
    private List<Scan> scans;
    
    @XmlElement(name="timestamp")
    private int timestamp;

    public ListScansResponse() {
    }
    
   
    
    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    public List<Scan> getScans() {
        return scans;
    }

    public void setScans(List<Scan> scans) {
        this.scans = scans;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ScanListResponse{" + "folders=" + folders + ", scans=" + scans + ", timestamp=" + timestamp + '}';
    }

   
    
    
}
