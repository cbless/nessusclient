/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.commands;

import de.cbless.nessusclient.NessusPostCommand;
import de.cbless.nessusclient.NessusModule;
import javax.ws.rs.client.Entity;

/**
 *
 * @author christoph
 */
//TODO: alt_targets als Entity
public class LaunchScanCommand extends NessusPostCommand{

    private int scanId;

    public LaunchScanCommand() {
    }

    public void setScanId(int scanId) {
        this.scanId = scanId;
    }

    public int getScanId() {
        return scanId;
    }
    
    
    @Override
    public Entity getEntity() {
        return Entity.json("{}");
    }

    @Override
    public String getPath() {
        return NessusModule.Scans.getPath() + "/" + scanId + "/launch";
    }
    
}
