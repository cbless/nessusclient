/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans.commands;

import de.cbless.nessusclient.NessusGetCommand;
import de.cbless.nessusclient.NessusModule;

/**
 *
 * @author Christoph Bless
 */
public class HostDetailsCommand extends NessusGetCommand {

    private int scanId;
    
    private int hostId;

    public int getScanId() {
        return scanId;
    }

    public void setScanId(int scanId) {
        this.scanId = scanId;
    }

    public int getHostId() {
        return hostId;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }
    
   
    
    @Override
    public String getPath() {
        return NessusModule.Scans.getPath() + "/" + scanId + "/hosts/" + hostId;
    }
    
}
