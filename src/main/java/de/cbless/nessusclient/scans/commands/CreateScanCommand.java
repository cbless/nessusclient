/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.commands;

import de.cbless.nessusclient.NessusPostCommand;
import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.scans.model.CreateScanRequest;
import de.cbless.nessusclient.scans.model.resources.Setting;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author christoph
 */
public class CreateScanCommand extends NessusPostCommand{

    private CreateScanRequest request;

    public CreateScanCommand() {
        request = new CreateScanRequest();
        request.setSettings(new Setting());
    }

    public String getTemplateUUID() {
        return request.getTemplateUUID();
    }

    public void setTemplateUUID(String templateUUID) {
        request.setTemplateUUID(templateUUID);
    }

    public String getScanName(){
        return request.getSettings().getName();
    }    
    
    public void setScanName(String name){
        request.getSettings().setName(name);
    }
    
    public void setTextTargets(String targets){
        request.getSettings().setTextTargets(targets);
    }
    
    public String getTextTargets(){
        return request.getSettings().getTextTargets();
    }
    
    
    public CreateScanRequest getRequest() {
        return request;
    }
       
    
    @Override
    public Entity getEntity() {
        return Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    public String getPath() {
        return NessusModule.Scans.getPath();
    }
    
}
