/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.commands;

import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.NessusGetCommand;

/**
 *
 * @author christoph
 */
public class ListScansCommand extends NessusGetCommand {

       
    public ListScansCommand() {
        super();
    }
    
    public void setFolderId(int folderId) {
        params.put("folder_id", folderId);
    }


    public void setLastModificationDate(int lastModificationDate) {
        params.put("last_modification_date", lastModificationDate);
    }

    @Override
    public String getPath() {
        return NessusModule.Scans.getPath();
    }
    
}
