/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans.commands;

import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.NessusGetCommand;

/**
 *
 * @author christoph
 */
public class ScanDetailsCommand extends NessusGetCommand{

    private int scanId;
    
    public ScanDetailsCommand() {
    }

    public void setScanId(int scanId) {
        this.scanId = scanId;
    }
    
    public void setHistoryId(int historyId) {
        params.put("history_id", historyId);
    }
    
    
    @Override
    public String getPath() {
        return NessusModule.Scans.getPath() + "/" + scanId;
    }

}
