/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.scans;

import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.NessusManager;
import de.cbless.nessusclient.NotFoundException;
import de.cbless.nessusclient.scans.commands.HostDetailsCommand;
import de.cbless.nessusclient.scans.commands.LaunchScanCommand;
import de.cbless.nessusclient.scans.commands.ScanDetailsCommand;
import de.cbless.nessusclient.scans.commands.ListScansCommand;
import de.cbless.nessusclient.scans.commands.PauseScanCommand;
import de.cbless.nessusclient.scans.commands.ResumeScanCommand;
import de.cbless.nessusclient.scans.commands.StopScanCommand;
import de.cbless.nessusclient.scans.model.HostDetailsResponse;
import de.cbless.nessusclient.scans.model.ScanDetailsResponse;
import de.cbless.nessusclient.scans.model.ListScansResponse;
import de.cbless.nessusclient.scans.model.LaunchScanResponse;
import de.cbless.nessusclient.scans.model.resources.Scan;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides the basic methods for managing scans and interacting 
 * with the scans on the Nessus server. All commands that are provided by this 
 * class require that the user is authenticated.
 * 
 * @author Christoph Bless
 */
public class ScanManager extends NessusManager {

    /**
     * Constructor. 
     * 
     * @param client Instance of a NessusClient
     */
    public ScanManager(NessusClient client) {
        super(client);
    }
    
    /**
     * List all available scans. 
     * 
     * This method lists all scans that are available for the loggedin user.
     * 
     * @return List of available scans.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public List<Scan> list()  throws NessusException {
        ListScansCommand cmd = new ListScansCommand();
        try{
            ListScansResponse response = client.get(cmd, ListScansResponse.class);
            return response.getScans();
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    
    /**
     * List of all available scans in the given folder. Only scans 
     * which are available for the logged in user will be in the resulting list.
     * 
     * @param folderId The id of the folder
     * @return List of available scans.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public List<Scan> listByFolder(int folderId)  
            throws NessusException {
        ListScansCommand cmd = new ListScansCommand();
        cmd.setFolderId(folderId);
        try{
            ListScansResponse response = client.get(cmd, ListScansResponse.class);
            return response.getScans();
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    /**
     * List of all available scans with the given name. Only scans 
     * which are available for the logged in user will be in the resulting list.
     * 
     * @param name The name of the scan to get
     * @return List of available scans.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public List<Scan> listByName(String name) 
            throws NessusException {
        if (name == null)
            throw new IllegalArgumentException("Parameter name must not be null!");
        List<Scan> result = new ArrayList<Scan>();
        // Get a list of all scans and then filter all scans which matches
        // the given name
        List<Scan> scans = list();
        for (Scan s : scans){
            if (name.equals(s.getName())){
                result.add(s);
            }
        }
        return result;
    }
    
    /**
     * Get the scan with the given id.
     * 
     * @param id The id of the scan to get.
     * @return The scan with the given id.
     * @throws NotFoundException A NotFoundException is thrown if there is no 
     *      scan with the specfied id.
     * @throws NessusException A NessusException is thrown if there was any 
     *      other error.
     */
    public Scan getScan(int id) 
            throws NotFoundException, NessusException {
        Scan scan = null;
        // Get a list of all scans and then lookup for the scan with the given 
        // id
        List<Scan> scans = list();
        for (Scan s : scans){
            if (s.getId() == id){
                scan = s;
            }
        }
        if (scan == null){
            throw new NotFoundException("Scan with id="+ id +" not found");
        }
        return scan;
    }
    
    
    /**
     * List of all available scans wich are in the given status. Only scans 
     * which are available for the logged in user will be in the resulting list.
     * 
     * @param status The status of the scans to get
     * @return List of available scans.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public List<Scan> listScansByStatus(String status) 
            throws NessusException {
        if (status == null)
            throw new IllegalArgumentException("Parameter status must not be null!");
        List<Scan> result = new ArrayList<Scan>();
        List<Scan> scans = list();
        for (Scan s : scans){
            if (status.equals(s.getStatus())){
                result.add(s);
            }
        }
        return result;
    }
    
    
    /**
     * This method returns details for the given scan.
     * 
     * @param scanId The id of the scan to retrieve.
     * @return Details of the given scan.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public ScanDetailsResponse details(int scanId) throws NessusException {
        ScanDetailsCommand cmd  = new ScanDetailsCommand();
        cmd.setScanId(scanId);
        try{
            ScanDetailsResponse response = client.get(cmd, ScanDetailsResponse.class);
            return response;        
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    
    /**
     * This method returns details for the given scan.
     * 
     * @param scanId The id of the scan to retrieve.
     * @param history_id The history_id of historical data that should be 
     *     returned.
     * @return Details of the given scan.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public ScanDetailsResponse details(int scanId, int history_id)  
            throws NessusException {
        ScanDetailsCommand cmd  = new ScanDetailsCommand();
        cmd.setScanId(scanId);
        cmd.setHistoryId(history_id);
        try{
            ScanDetailsResponse response = client.get(cmd, ScanDetailsResponse.class);
            return response;  
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    /**
     * This method starts an existing scan.
     * 
     * @param scanId The id of the scan to pause.
     * @return The id of the scan used for requesting results.
     * @throws NessusException Throws a NessusException if an error occures.
     */ 
    public String launch(int scanId) throws NessusException {
        LaunchScanCommand cmd = new LaunchScanCommand();
        cmd.setScanId(scanId);
        try{
            LaunchScanResponse response = client.post(cmd, LaunchScanResponse.class);
            return response.getScanUUID();
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    /** 
     * Pauses an existing scan, allowing it to be resumed at a later time. 
     * 
     * @param scanId The id of the scan to pause.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public void pause(int scanId) throws NessusException {
        PauseScanCommand cmd = new PauseScanCommand();
        cmd.setScanId(scanId);
        try{
            client.postAndCheckResponse(cmd, 200);
        } catch(Exception ex ){
            throw new NessusException(ex);
        } 
    }
    
    /**
     * Resumes a previously paused scan. 
     * 
     * @param scanId The id of the scan to resume.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public void resume(int scanId) throws NessusException {
        ResumeScanCommand cmd = new ResumeScanCommand();
        cmd.setScanId(scanId);
        
        try{
            client.postAndCheckResponse(cmd, 200);
        } catch(Exception ex ){
            throw new NessusException(ex);
        }
    }
    
    /**
     * Stops an existing scan. 
     * 
     * @param scanId The id of the scan to stop.
     * @throws NessusException Throws a NessusException if an error occures.
     */
    public void stop(int scanId) throws NessusException {
        StopScanCommand cmd = new StopScanCommand();
        cmd.setId(scanId);
        try{
            client.postAndCheckResponse(cmd, 200);
        } catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    /**
     * This method returns details for the given host.
     * 
     * @param scanId The id of the scan to retrieve.
     * @param hostId The id of the host to retrieve
     * @return Details for the given host.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public HostDetailsResponse hostDetails(int scanId, int hostId) 
            throws NessusException {
        HostDetailsCommand cmd = new HostDetailsCommand();
        cmd.setScanId(scanId);
        cmd.setHostId(hostId);
        try{
            return client.get(cmd, HostDetailsResponse.class);
        } catch(Exception ex){
            throw new NessusException(ex);
        }
    }
}
