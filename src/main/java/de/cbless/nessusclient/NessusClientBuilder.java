/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 * This class creates instances of the class NessusClient.
 * 
 * @author Christoph Bless
 */
public class NessusClientBuilder {

    private String url;
    private boolean trustAll = false;

    // Create a trust manager that does not validate certificate chains
    private static TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

    // Ignore differences between given hostname and certificate hostname
    private static final HostnameVerifier hv = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    
    public NessusClientBuilder(String url) {
        this.url = url;
    }
    
    public NessusClientBuilder(String url, boolean trustAll) {
        this.url = url;
        this.trustAll = trustAll;
    }
    

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    private Client buildClient() throws NoSuchAlgorithmException, KeyManagementException{
        if (trustAll){            
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            return ClientBuilder.newBuilder().sslContext(sc)
                    .hostnameVerifier(hv).build();
        }else{
            return ClientBuilder.newClient();
        }
    }
    
    /**
     * Creates an instance of the class NessusClient.
     * 
     * @return
     * @throws NoSuchAlgorithmException This exception is thrown cryptographic algorithm is requested but is not available in the environment.
     * @throws KeyManagementException 
     */
    public NessusClient build() throws NoSuchAlgorithmException, KeyManagementException {
        Client client = buildClient();
        return new NessusClient(client, url);
    }
}
