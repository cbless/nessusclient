/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a abstract class for Nessus commands which uses a HTTP-GET request. 
 * 
 * @author Christoph Bless
 */
public abstract class NessusGetCommand implements NessusCommand{

    protected Map<String, Object> params;

    public NessusGetCommand() {
        params = new HashMap<String, Object>();
    }
    
    
    /**
     * Returns a map of Parameters which will be added as Query-Parameters. 
     * 
     * @return A map with Query-Parameters
     */
    public Map<String, Object> getQueryParameters() {
        return params;
    }
    
    
}
