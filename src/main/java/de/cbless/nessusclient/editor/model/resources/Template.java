/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.editor.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Templates are used to create scans or policies with predefined parameters.
 * 
 * @author Christop Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Template {
    
    /** The uuid for the template */
    @XmlElement
    private String uuid;
    
    /** The short name of the template. */
    @XmlElement
    private String name;
    
    /** The long name of the template. */
    @XmlElement
    private String title;
    
    /** The description of the template. */
    @XmlElement
    private String description;
    
    /** If true, template is only available on the cloud. */
    @XmlElement(name="cloud_only")
    private Boolean cloudOnly;
    
    /** If true, the template is only available for subscribers. */
    @XmlElement(name ="subscription_only")
    private Boolean subscriptionOnly;
    
    /** If true, the template is for agent scans. */
    @XmlElement(name ="is_agent")
    private Boolean isAgent;
    
    /** An external URL to link the template to. */
    @XmlElement(name = "more_info")
    private String moreInfo;

    public Template() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCloudOnly() {
        return cloudOnly;
    }

    public void setCloudOnly(Boolean cloudOnly) {
        this.cloudOnly = cloudOnly;
    }

    public Boolean getSubscriptionOnly() {
        return subscriptionOnly;
    }

    public void setSubscriptionOnly(Boolean subscriptionOnly) {
        this.subscriptionOnly = subscriptionOnly;
    }

    public Boolean getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Boolean isAgent) {
        this.isAgent = isAgent;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    @Override
    public String toString() {
        return "Template{" + "uuid=" + uuid + ", name=" + name + ", title=" + title + ", description=" + description + ", cloudOnly=" + cloudOnly + ", subscriptionOnly=" + subscriptionOnly + ", isAgent=" + isAgent + ", moreInfo=" + moreInfo + '}';
    }
    
    
}
