/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.editor.commands;

import de.cbless.nessusclient.NessusGetCommand;
import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.editor.model.TemplateType;

/**
 *
 * @author Christoph Bless
 */
public class ListTemplatesCommand extends NessusGetCommand {
    
    private TemplateType type;

    public ListTemplatesCommand() {
    }

    public void setType(TemplateType type) {
        this.type = type;
    }

    public TemplateType getType() {
        return type;
    }
        
    @Override
    public String getPath() {
        return NessusModule.Editor.getPath() + "/" + type.getName() + "/templates";
    }
    
}
