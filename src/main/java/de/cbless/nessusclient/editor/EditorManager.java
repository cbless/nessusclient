/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.editor;

import de.cbless.nessusclient.editor.model.TemplateType;
import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusManager;
import de.cbless.nessusclient.editor.commands.ListTemplatesCommand;
import de.cbless.nessusclient.editor.model.ListTemplatesResponse;
import de.cbless.nessusclient.editor.model.resources.Template;
import java.util.List;

/**
 * This class provides access to the commands of editor module.
 * 
 * @author Christoph Bless
 */
public class EditorManager extends NessusManager {

    public EditorManager(NessusClient client) {
        super(client);
    }
    
    /**
     * 
     * 
     * Permissions:
     * 
     * 
     * Example: 
     * 
     * 
     * @param type
     * @return 
     */
    public List<Template> list(TemplateType type) {
        ListTemplatesCommand cmd = new ListTemplatesCommand();
        cmd.setType(type);
        ListTemplatesResponse response = client.get(cmd, ListTemplatesResponse.class);
        return response.getTemplates();
    }
}
