/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.plugins.commands;

import de.cbless.nessusclient.NessusGetCommand;
import de.cbless.nessusclient.NessusModule;

/**
 *
 * @author Christoph Bless
 */
public class PluginDetailsCommand extends NessusGetCommand {

    private int id;
    
    public PluginDetailsCommand() {
        super();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
        
    @Override
    public String getPath() {
        return NessusModule.Plugins.getPath() + "/plugin/" + id;
    }
    
    
}
