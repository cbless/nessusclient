/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This package contains the ressource types that are defined in the plugins 
 * module. The descriptions of all Classes in this package are taken from the 
 * Nessus API. 
 * @see https://127.0.0.1:8834/nessus6-api.html#/resources/plugins
 * (replace the IP and Port specification to parameters which match 
 * to your Nessus installation)
 */
package de.cbless.nessusclient.plugins.model.resources;
