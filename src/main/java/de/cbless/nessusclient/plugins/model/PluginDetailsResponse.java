/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.plugins.model;

import de.cbless.nessusclient.plugins.model.resources.Attribute;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christoph Bless
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginDetailsResponse {
    
    @XmlElement
    private int id;
    
    @XmlElement    
    private String name;
    
    @XmlElement(name="family_name")
    private String familyName;
    
    @XmlElement
    private List<Attribute> attributes;

    public PluginDetailsResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "PluginDetailsResponse{" + "id=" + id + ", name=" + name + ", familyName=" + familyName + ", attributes=" + attributes + '}';
    }
    
    
}
