/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.plugins;

import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.NessusManager;
import de.cbless.nessusclient.plugins.commands.FamilyDetailsCommand;
import de.cbless.nessusclient.plugins.commands.ListFamiliesCommand;
import de.cbless.nessusclient.plugins.commands.PluginDetailsCommand;
import de.cbless.nessusclient.plugins.model.PluginDetailsResponse;
import de.cbless.nessusclient.plugins.model.resources.Family;
import de.cbless.nessusclient.plugins.model.FamilyDetailsResponse;
import java.util.List;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 * This class provides methods to request information about plugins and plugin-
 * families. 
 * 
 * @author Christoph Bless
 */
public class PluginManager extends NessusManager {

    public PluginManager(NessusClient client) {
        super(client);
    }
    
    
    /**
     * Returns the details for a given plugin. This method requires the id of 
     * the plugin as parameter. <br /><br />
     * 
     * Permissions: <br /> 
     * <ul> 
     *  <li>authenticated: Yes</li>
     *  <li>administrator: No</li>
     * </ul>
     * <br />
     * Example:<br />
     * <pre>
     * <code>
     * try{
     *      NessusClient nessusClient = new NessusClient("https://127.0.0.1:8834", true);
     *      nessusClient.login("testuser", "testuser");
     *      PluginManager pm = new PluginManager(nessusClient);
     *      PluginDetailsResponse response  = pm.getDetails(42);
     *      String family = resonse.getFamilyName();
     *      String name = response.getName();
     *      List &lt;Attributes&gt; attributes = response.getAttributes();
     *      // ...
     * } catch(SessionException ex){
     *      // ...
     * } catch(NessusException ex){
     *      // ...
     * }
     * </code>
     * </pre>
     * @param id Id of the plugin.
     * @return Returns the list of plugin families. 
     * @throws NessusException Throws a NessusException if any error occurs. 
     */
    public PluginDetailsResponse getDetails(int id) throws NessusException{
        PluginDetailsCommand cmd = new PluginDetailsCommand();
        cmd.setId(id);
        try{
            PluginDetailsResponse response = client.get(cmd, 
                    PluginDetailsResponse.class);
            return response;
        } catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    /**
     * Returns the list of plugin families. This method does not have any 
     * parameters. <br /><br />
     * 
     * Permissions: <br /> 
     * <ul> 
     *  <li>authenticated: Yes</li>
     *  <li>administrator: No</li>
     * </ul>
     * <br />
     * Example:<br />
     * <pre>
     * <code>
     * try{
     *      NessusClient nessusClient = new NessusClient("https://127.0.0.1:8834", true);
     *      nessusClient.login("testuser", "testuser");
     *      PluginManager pm = new PluginManager(nessusClient);
     *      List &lt;Family&gt; families = pm.listFamilies();
     *      // ...
     * } catch(SessionException ex){
     *      // ...
     * } catch(NessusException ex){
     *      // ...
     * }
     * </code>
     * </pre>
     * 
     * @return Returns the list of plugin families. 
     * @throws NessusException Throws a NessusException if any error occurs. 
     */
    public List<Family> listFamilies() throws NessusException {
        ListFamiliesCommand cmd = new ListFamiliesCommand();
        try{
            Response response = client.get(cmd, Response.class);
            try{
                List<Family> families = response.readEntity(new GenericType<List<Family>>(){});
                return families;
            } finally{
                response.close();
            }
        }catch(Exception ex){
            throw new NessusException(ex);
        } 
    }
    
    
    /**
     * Returns the list of plugins in a family . This method requires the id of 
     * the family as parameter. <br /><br />
     * 
     * Permissions: <br /> 
     * <ul> 
     *  <li>authenticated: Yes</li>
     *  <li>administrator: No</li>
     * </ul>
     * <br />
     * Example:<br />
     * <pre>
     * <code>
     * try{
     *      NessusClient nessusClient = new NessusClient("https://127.0.0.1:8834", true);
     *      nessusClient.login("testuser", "testuser");
     *      PluginManager pm = new PluginManager(nessusClient);
     *      FamilyDetailsResponse response  = pm.getFamilyDetails(42);
     *      String family = resonse.getName();
     *      List &lt;Plugin&gt; plugins = response.getPlugins();
     *      // ...
     * } catch(SessionException ex){
     *      // ...
     * } catch(NessusException ex){
     *      // ...
     * }
     * </code>
     * </pre>
     * 
     * @param id The id of the family to lookup.
     * @return Returns the list of plugin families. 
     * @throws NessusException Throws a NessusException if any error occurs. 
     */
    public FamilyDetailsResponse getFamilyDetails(int id) 
            throws NessusException {
        FamilyDetailsCommand cmd = new FamilyDetailsCommand();
        cmd.setId(id);
        try{
            FamilyDetailsResponse response = client.get(cmd, 
                    FamilyDetailsResponse.class);
            return response;
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    
}
