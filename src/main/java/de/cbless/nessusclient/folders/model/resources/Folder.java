/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folders.model.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Folders are used to sort and organize a user's scan results.
 * 
 * @author Christoph
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Folder {
    
    /** The unique id of the folder */
    @XmlElement
    private int id;

    /** The name of the folder */
    @XmlElement
    private String name;
    
    /** The type of the folder (main, trash, custom) */
    @XmlElement
    private String type;
    
    /** Whether or not the folder is the default (1 or 0) */
    @XmlElement(name = "default_tag")
    private int defaultTag;
    
    /** The custom status of the folder (1 or 0) */
    @XmlElement
    private int custom;
    
    /** The number of unread scans in the folder */
    @XmlElement(name = "unread_count")
    private int unreadCount;

    public Folder() {
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDefaultTag() {
        return defaultTag;
    }

    public void setDefaultTag(int defaultTag) {
        this.defaultTag = defaultTag;
    }

    public int getCustom() {
        return custom;
    }

    public void setCustom(int custom) {
        this.custom = custom;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    @Override
    public String toString() {
        return "Folder{" + "id=" + id + ", name=" + name + ", type=" + type + ", defaultTag=" + defaultTag + ", custom=" + custom + ", unreadCount=" + unreadCount + '}';
    }
    
    
    
    
    
 
} 
