/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folders.commands;

import de.cbless.nessusclient.NessusGetCommand;
import de.cbless.nessusclient.NessusModule;

/**
 *
 * @author christoph
 */
public class ListFoldersCommand extends NessusGetCommand {

    public ListFoldersCommand() {
        super();
    }

    @Override
    public String getPath() {
        return NessusModule.Folders.getPath();
    }
    
    
    
    
}
