/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folders.commands;

import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.NessusPutCommand;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author christoph
 */
public class EditFolderCommand extends NessusPutCommand {

    private static final String NAME_KEY = "name";
    private int folderId; 
    private String folderName;

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public String getFolderName() {
        return params.getFirst(NAME_KEY);
    }

    public void setFolderName(String folderName) {
        params.putSingle(NAME_KEY, folderName);
    }
    
    
    @Override
    public String getPath() {
        return NessusModule.Folders.getPath() + "/" + folderId;
    }

    
}
