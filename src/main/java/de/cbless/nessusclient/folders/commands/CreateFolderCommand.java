/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folders.commands;

import de.cbless.nessusclient.NessusPostCommand;
import de.cbless.nessusclient.NessusModule;
import de.cbless.nessusclient.folders.model.CreateFolderRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author christoph
 */
public class CreateFolderCommand extends NessusPostCommand {

    private CreateFolderRequest request = new CreateFolderRequest();

    public void setName(String name) {
        request.setName(name);
    }
    
    @Override
    public String getPath() {
        return NessusModule.Folders.getPath();
    }

    @Override
    public Entity getEntity() {
        return Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);
    }
    
}
