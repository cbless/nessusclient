/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folders.commands;

import de.cbless.nessusclient.NessusDeleteCommand;
import de.cbless.nessusclient.NessusModule;

/**
 *
 * @author christoph
 */
public class DeleteFolderCommand extends NessusDeleteCommand {

    private int folderId;

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getFolderId() {
        return folderId;
    }
    
    
    @Override
    public String getPath() {
        return NessusModule.Folders.getPath() + "/" + folderId;
    }
    
}
