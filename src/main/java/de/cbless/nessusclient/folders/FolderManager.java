/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient.folders;

import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.NessusManager;
import de.cbless.nessusclient.NotFoundException;
import de.cbless.nessusclient.folders.commands.CreateFolderCommand;
import de.cbless.nessusclient.folders.commands.DeleteFolderCommand;
import de.cbless.nessusclient.folders.commands.EditFolderCommand;
import de.cbless.nessusclient.folders.commands.ListFoldersCommand;
import de.cbless.nessusclient.folders.model.CreateFolderResponse;
import de.cbless.nessusclient.folders.model.ListFoldersResponse;
import de.cbless.nessusclient.folders.model.resources.Folder;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides access to the commands of the folder module of the 
 * Nessus server.
 * 
 * @author Christoph Bless
 */
public class FolderManager extends NessusManager {

    public FolderManager(NessusClient client) {
        super(client);
    }
    
    
    /**
     * Creates a new folder for the current user.
     * 
     * @param name The name of the folder.
     * @return The id of the created folder.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public int create(String name) throws NessusException {
        CreateFolderCommand cmd = new CreateFolderCommand();
        cmd.setName(name);
        try{
            CreateFolderResponse response = client.post(cmd, CreateFolderResponse.class);
            return response.getId();
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    
    /**
     * Returns a list of the current users scan folders.
     * 
     * @return A list of folders.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public List<Folder> list() throws NessusException {
        ListFoldersCommand cmd = new ListFoldersCommand();
        try{
            ListFoldersResponse response = client.get(cmd, ListFoldersResponse.class);
            return response.getFolders();
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    } 
    
    
    /**
     * Returns a single folder. 
     * 
     * @param id The id of the folder to get.
     * @return A single folder with the given id.
     * @throws NotFoundException 
     * @throws NessusException Throws a NessusException if any other error occurs.
     */
    public Folder listById(int id) throws NessusException, NotFoundException {
        List<Folder> folders = list();
        Folder result = null;
        for (Folder f : folders){
            if (f.getId() == id){
                result = f;
            }
        }
        if (result == null){
            throw new NotFoundException("Folder ("+ id +") not found!");
        }
        return result;
    }   
    
    
    /**
     * Returns a list of folders.
     * 
     * @param name The name of the folder to get.
     * @return A list of folders with the given name.
     * @throws NessusException Throws a NessusException if any other error occurs.
     */
    public List<Folder> listByName(String name) throws NessusException {
        if (name == null)
            throw new IllegalArgumentException("Parameter name must not be null!");
        List<Folder> folders = list();
        List<Folder> result = new ArrayList<Folder>();
        for (Folder f : folders){
            if (name.equals(f.getName())){
                result.add(f);
            }
        }
        return result;
    }
    
    
    /**
     * Renames a scan folder for the current user.
     * 
     * @param id The id of the folder to edit.
     * @param name The new name of the folder.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public void edit(int id, String name) throws NessusException {
        EditFolderCommand cmd = new EditFolderCommand();
        cmd.setFolderId(id); 
        cmd.setFolderName(name);
        try{
            client.put(cmd);
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
    
    
    /**
     * Deletes a scan folder.
     * 
     * @param folderId The id of the folder to delete.
     * @throws NessusException Throws a NessusException if any error occurs.
     */
    public void delete(int folderId) throws NessusException {
        DeleteFolderCommand cmd = new DeleteFolderCommand();
        cmd.setFolderId(folderId);
        try{
            client.delete(cmd);
        }catch(Exception ex){
            throw new NessusException(ex);
        }
    }
}
