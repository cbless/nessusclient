/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

/**
 * This enum defines the pathname for each Nessus module. This names should be 
 * used as a part of the query URL. 
 * 
 * @author Christoph Bless
 */
public enum NessusModule {
    Editor("editor"),
    Folders("folders"),
    Scans("scans"),
    Session("session"),
    Users("users"),
    Policies("policies"),
    PluginRules("plugin-rules"),
    Plugins("plugins"),
    Scanners("scanners");    
    
    private final String path;

    private NessusModule(String path) {
        this.path = path;
    }

    /** 
     * This method returns the pathname for the Nessus module that should be 
     * used in the URL.
     * 
     * @return pathname as String
     */
    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return path;
    }
    
    
    
}
