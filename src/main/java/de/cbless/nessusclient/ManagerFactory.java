/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

import de.cbless.nessusclient.editor.EditorManager;
import de.cbless.nessusclient.folders.FolderManager;
import de.cbless.nessusclient.plugins.PluginManager;
import de.cbless.nessusclient.scans.ScanManager;
import de.cbless.nessusclient.session.SessionManager;

/**
 * This class creates instances of the Manager classes.
 * 
 * @author Christoph Bless
 */
public class ManagerFactory {
    
    private final NessusClient client;

    public ManagerFactory(NessusClient client) {
        this.client = client;
    }
    
    /**
     * Creates an instance of the ScanManager
     * 
     * @return instance of ScanManager
     */
    public ScanManager createScanManager(){
        return new ScanManager(client);
    }
    
    
    /**
     * Creates an instance of the EditorManager
     * 
     * @return instance of EditorManager
     */
    public EditorManager createEditorManager(){
        return new EditorManager(client);
    }
    
    
    /**
     * Creates an instance of the FolderManager
     * 
     * @return instance of FolderManager
     */
    public FolderManager createFolderManager(){
        return new FolderManager(client);
    }
    
    
    /**
     * Creates an instance of the PluginManager
     * 
     * @return instance of PluginManager
     */
    public PluginManager createPluginManager(){
        return new PluginManager(client);
    }
    
    
    /**
     * Creates an instance of the SessionManager
     * 
     * @return instance of SessionManager
     */
    public SessionManager createSessionManager(){
        return new SessionManager(client);
    }
    
}
