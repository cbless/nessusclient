/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

import de.cbless.nessusclient.session.SessionManager;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This class provides the basic functionality to communicate with the Nessus 
 * Server. This includes commands like GET, POST, PUT or DELETE.
 * 
 * @author Christoph Bless
 */
public class NessusClient {
    
    /** The name of the Cookie which must be set after a successful login */
    private static final String SESSION_NAME = "X-COOKIE";
    /** The prefix for the session token */
    private static final String SESSION_VALUE_PREFIX = "token=";
    
    private final WebTarget webTarget;
    private final Client client;
    private SessionManager sessionManager;
    private String token = null;
    
    
    public NessusClient(Client client, String url){
        this.client = client;
        webTarget = client.target(url);
    }
        
    
    /** 
     * Send a command via the HTTP-Post method to the Nessus server. 
     * 
     * <br /><br />
     * Example: <br />
     * <pre><code>
     *  NessusClient client;
     *  // ... create and initialize client, login user
     *  LaunchScanCommand cmd = new LaunchScanCommand();
     *  cmd.setScanId(scanId);
     *  try{
     *      LaunchScanResponse response = client.post(cmd, LaunchScanResponse.class);
     *      // ...
     *  }catch(NessusException ex){
     *      // ..
     *  }
     * </code></pre>
     * 
     * @param <T> Class of the return type
     * @param cmd Instance of a NessusPostCommand, that should be executed
     * @param retType Class of the return type
     * @return server response
     * @throws WebApplicationException  Throws a WebApplicationException if any 
     *      error occurs.
     */
    public <T> T post(NessusPostCommand cmd, Class<T> retType) 
            throws WebApplicationException{
        WebTarget target = webTarget.path(cmd.getPath());
        T response;
        if ( isAuthenticated()  ){
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .header(SESSION_NAME, SESSION_VALUE_PREFIX + token)
                    .post(cmd.getEntity(), retType);
        }else{
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .post(cmd.getEntity(), retType);
        }        
        return response;
    }
    
    
    /** 
     * Send a command via the HTTP-Post method to the Nessus server and check 
     * the response code. This method will throw a WebApplicationException if 
     * the response code doesn't match the expected response code.
     * 
     * <br /><br />
     * Example: <br />
     * <pre><code>
     *  NessusClient client;
     *  // ... create and initialize client, login user
     *  PauseScanCommand cmd = new PauseScanCommand();
     *  cmd.setScanId(scanId);
     *  try{
     *      client.postAndCheckResponse(cmd, 200);
     *      // ... 
     *  } catch(WebApplicationException ex ){
     *      // ...
     *  } 
     * </code></pre>
     * 
     * @param cmd Instance of a NessusPostCommand, that should be executed
     * @param responseCode  Expected response code
     * @throws WebApplicationException  Throws a WebApplicationException if any 
     *      error occurs.
     */
    public void postAndCheckResponse(NessusPostCommand cmd, int responseCode) 
            throws WebApplicationException{
        WebTarget target = webTarget.path(cmd.getPath());
        Response response;
        if ( isAuthenticated()  ){
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .header(SESSION_NAME, SESSION_VALUE_PREFIX + token)
                    .post(cmd.getEntity());
        }else{
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .post(cmd.getEntity());
        }        
        try{
            if (response.getStatus() != responseCode){
                throw new WebApplicationException(response);
            }
        } finally{
            response.close();
        }
    }
    
    
    /**
     * Request a ressouce on the server via a HTTP-GET request. This method will 
     * throw a WebApplicationException if there are any errors with the request.
     * 
     * @param <T> Return type
     * @param cmd Command that should be send to the server
     * @param retType Class of the return type
     * @return server response
     * @throws WebApplicationException Throws a WebApplicationException if any 
     *      error occurs.
     */
    public <T> T get(NessusGetCommand cmd, Class<T> retType) throws WebApplicationException{
        WebTarget target = webTarget.path(cmd.getPath());
        T response;
        for (String key : cmd.getQueryParameters().keySet()){
            target = target.queryParam(key, cmd.getQueryParameters().get(key));
        }
        if ( isAuthenticated()  ){
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .header(SESSION_NAME, SESSION_VALUE_PREFIX + token)
                    .get(retType);
        }else{
            response = target.request(MediaType.APPLICATION_JSON_TYPE)
                    .get(retType);
        }
        return response;
        
    }
    
    
    /**
     * Send data to the server via a HTTP-PUT method. This method should throw 
     * an WebApplicationException if any error occurs.
     * @param cmd The command that should be send to the server
     * @throws WebApplicationException Throws a WebApplicationException if any 
     *      error occurs.
     */
    public void put(NessusPutCommand cmd) throws WebApplicationException {
        WebTarget target = webTarget.path(cmd.getPath());
        Response response = null;
   
        try{ 
            if ( isAuthenticated() ){
                response = target.request(MediaType.APPLICATION_JSON_TYPE)
                        .header(SESSION_NAME, SESSION_VALUE_PREFIX + token)
                        .put(cmd.getEntity());
            }else{
                response = target.request(MediaType.APPLICATION_JSON_TYPE)
                        .put(cmd.getEntity());
            }    
            if (response.getStatus() != 200 ){
                throw new WebApplicationException(response);
            }
        } finally{
            if (response != null){
                response.close();
            }
        }
        
        
    }
    
    /**
     * Send data to the server via a HTTP-DELETE method.This method should throw 
     * an WebApplicationException if any error occurs.
     * 
     * @param cmd The command that should be send to the server
     * @throws WebApplicationException Throws a WebApplicationException if any 
     *      error occurs.
     */
    public void delete(NessusDeleteCommand cmd) throws WebApplicationException {
        WebTarget target = webTarget.path(cmd.getPath());
        Response response = null;
        
        try{ 
            if ( isAuthenticated() ){
                response = target.request(MediaType.APPLICATION_JSON_TYPE)
                        .header(SESSION_NAME, SESSION_VALUE_PREFIX + token)
                        .delete();
            }else{
                response = target.request(MediaType.APPLICATION_JSON_TYPE).delete();
            }        
            if (response.getStatus() != 200 ){
                throw new WebApplicationException(response);
            }
        } finally{
            if (response != null){
                response.close();
            }
        }
    }
    
    /**
     * This method allows to authenticate to the server.
     * 
     * @param username The username to use.
     * @param password The password to use for the user.
     * @throws SessionException This exception is thrown if the username or 
     *      password was invalid or if too many users are connected.
     * @throws NessusException If any other error occurs.
     */
    public void login(String username, String password) 
            throws NessusException, SessionException {
        if (sessionManager == null){
            sessionManager = new ManagerFactory(this).createSessionManager();
        }
        token = sessionManager.doLogin(username, password);
    }
        
    /**
     * This methods checks if the user is authenticated.
     * @return <code>true</code> if the user is authenticated, 
     *      <code>false</code> otherwise.
     */
    public boolean isAuthenticated(){
        return token != null;
    }
    
    
    /**
     * This method is used to logout the user.
     * @throws SessionException This exception is thrown if the logout was not 
     *      successful
     * @throws NessusException  This exception is thrown if any other error 
     *      occurs.
     */
    public void logout() throws NessusException, SessionException {
        if (sessionManager == null){
            sessionManager = new ManagerFactory(this).createSessionManager();
        }
        sessionManager.doLogout();
        token = null;
    }
}
