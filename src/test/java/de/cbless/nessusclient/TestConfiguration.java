/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient;

/**
 *
 * @author christoph
 */
public class TestConfiguration {
    
    public static final String NESSUS_URL = "https://127.0.0.1:8834";
    
    public static final String NESSUS_USER = "testuser";
    
    public static final String NESSUS_UNKNOWN_USER = "unknownuser";
    
    public static final String NESSUS_PASS = "testuser";
    
    public static final int EXISTING_SCAN_ID = 110;
    
    public static final String EXISTING_SCAN_NAME = "XP_Basic";
}
