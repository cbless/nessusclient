/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.editor;

import de.cbless.nessusclient.ManagerFactory;
import de.cbless.nessusclient.editor.model.TemplateType;
import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusClientBuilder;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.SessionException;
import de.cbless.nessusclient.TestConfiguration;
import de.cbless.nessusclient.editor.model.resources.Template;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author christoph
 */
public class EditorManagerTest {
    
    private NessusClient nessusClient;
    private EditorManager editorManager;
    
    public EditorManagerTest() throws NoSuchAlgorithmException, KeyManagementException {
        NessusClientBuilder builder = new NessusClientBuilder(TestConfiguration.NESSUS_URL, true);        
        nessusClient = builder.build();
        try {
            nessusClient.login(TestConfiguration.NESSUS_USER, 
                    TestConfiguration.NESSUS_PASS);
            editorManager = new ManagerFactory(nessusClient).createEditorManager();
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (NessusException ex ){
            fail(ex.getMessage());
        }   
    }
    
    
    /**
     * Test of list method, of class EditorManager.
     */
    @Test
    public void testListScans() {
        List<Template> templates = editorManager.list(TemplateType.Scan);
        for (Template t : templates){
            System.out.println(t);
        }
    }
    
    @Test
    public void testListPolicies() {
        List<Template> templates = editorManager.list(TemplateType.Policy);
        for (Template t : templates){
            System.out.println(t);
        }
    }
}
