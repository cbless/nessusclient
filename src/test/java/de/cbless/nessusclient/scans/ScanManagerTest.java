/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.scans;

import de.cbless.nessusclient.ManagerFactory;
import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusClientBuilder;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.SessionException;
import de.cbless.nessusclient.TestConfiguration;
import de.cbless.nessusclient.scans.model.HostDetailsResponse;
import de.cbless.nessusclient.scans.model.ScanDetailsResponse;
import de.cbless.nessusclient.scans.model.resources.Scan;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author christoph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScanManagerTest {
    
    private NessusClient nessusClient;
    private ScanManager scanManager;
    
    public ScanManagerTest() throws NoSuchAlgorithmException, KeyManagementException {
        NessusClientBuilder builder = new NessusClientBuilder(TestConfiguration.NESSUS_URL, true);        
        nessusClient = builder.build();
        try {
            nessusClient.login(TestConfiguration.NESSUS_USER, 
                    TestConfiguration.NESSUS_PASS);
            scanManager = new ManagerFactory(nessusClient).createScanManager();
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (NessusException ex ){
            fail(ex.getMessage());
        }        
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of listScans method, of class ScanManager.
     */
    @Test
    public void testListScans() {
        List<Scan> response = scanManager.list();
        // TODO
    }

    

    
    /**
     * Test of details method, of class ScanManager.
     */
    @Test
    public void testDetails_int() {
        int scanId = 110;
        ScanDetailsResponse response = scanManager.details(scanId);
        System.out.println(response);
    }

    
    @Test
    public void testLaunch(){
        String uuid = scanManager.launch(110);
        System.out.println(uuid);
    }
    
    @Test
    public void testPause(){
        scanManager.pause(110);
    }
    
    @Test
    public void testResume(){
        scanManager.resume(110);
    }
    
    @Test
    public void testStop(){
        scanManager.stop(110);
    }

    
    /**
     * Test of hostDetails method, of class ScanManager.
     */
    @Test
    public void testHostDetails() {
        System.out.println("hostDetails");
        HostDetailsResponse response = scanManager.hostDetails(209, 2);
        System.out.println(response);
    }
    
}
