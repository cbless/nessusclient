/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.plugins;

import de.cbless.nessusclient.ManagerFactory;
import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusClientBuilder;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.SessionException;
import de.cbless.nessusclient.TestConfiguration;
import de.cbless.nessusclient.plugins.model.PluginDetailsResponse;
import de.cbless.nessusclient.plugins.model.resources.Family;
import de.cbless.nessusclient.plugins.model.FamilyDetailsResponse;
import de.cbless.nessusclient.plugins.model.resources.Plugin;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import static org.hamcrest.Matchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PluginManagerTest {
    
    private NessusClient nessusClient;
    private PluginManager pluginManager;
    
    private static int lookupFamilyId;
    private static int lookupPluginId;
    
    private static final String LOOKUP_PLUGIN_FAMILY = "Windows";
    private static final String LOOKUP_PLUGIN_NAME = "MS09-001: Microsoft Windows SMB Vulnerabilities Remote Code Execution (958687) (uncredentialed check)";
    
    
    public PluginManagerTest() throws NoSuchAlgorithmException, KeyManagementException {
        NessusClientBuilder builder = new NessusClientBuilder(TestConfiguration.NESSUS_URL, true);        
        nessusClient = builder.build();
        try {
            nessusClient.login(TestConfiguration.NESSUS_USER, 
                    TestConfiguration.NESSUS_PASS);
            pluginManager = new ManagerFactory(nessusClient).createPluginManager();
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (NessusException ex ){
            fail(ex.getMessage());
        }  
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test1ListFamilies(){
        System.out.println("listFamilies");
        try{
            List<Family> families = pluginManager.listFamilies();
            assertTrue(families.size() > 0);
            for (Family f : families){
                if (f.getName().equals(LOOKUP_PLUGIN_FAMILY)){
                    System.out.println(f.getId() + " "+f.getName());
                    lookupFamilyId = f.getId();
                }
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    @Test
    public void test2GetFamilyDetails(){
        System.out.println("getFamilyDetails");
        System.out.println("lookupFamilyID: "+ lookupFamilyId);
        FamilyDetailsResponse response = pluginManager.getFamilyDetails(lookupFamilyId);
        assertThat(response.getName(), equalTo(LOOKUP_PLUGIN_FAMILY));
        assertTrue(response.getPlugins().size() > 0 );
        for (Plugin p : response.getPlugins()){
            if (p.getName().equals(LOOKUP_PLUGIN_NAME)){
                lookupPluginId = p.getId();
            }
        }
    }
    
    /**
     * Test of getDetails method, of class PluginManager.
     */
    @Test
    public void test3GetDetails() {
        System.out.println("getDetails");
        PluginDetailsResponse response = pluginManager.getDetails(lookupPluginId);
        assertThat(lookupPluginId, equalTo(response.getId()));
        assertThat(LOOKUP_PLUGIN_NAME, equalTo(response.getName()));
        assertThat(LOOKUP_PLUGIN_FAMILY , equalTo(response.getFamilyName()));
        
    }
}
