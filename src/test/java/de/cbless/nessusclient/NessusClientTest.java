/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.nessusclient;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NessusClientTest {
    
    private static NessusClient client;
    
    public NessusClientTest() throws NoSuchAlgorithmException, KeyManagementException {
        NessusClientBuilder builder = new NessusClientBuilder(TestConfiguration.NESSUS_URL, true);        
        client = builder.build();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of login method, of class NessusClient.
     */
    @Test
    public void test01LoginError() {
        System.out.println("loginError");
        try{
            client.login(TestConfiguration.NESSUS_UNKNOWN_USER, 
                    TestConfiguration.NESSUS_PASS);
        } catch (SessionException ex){
            if (client.isAuthenticated()){
                fail("isAuthenticated should return false");
            }
        } catch(NessusException ex){
            fail(ex.getMessage());
        }
    }
    
    /**
     * Test of login method, of class NessusClient.
     */
    @Test
    public void test01LoginSuccessful() {
        System.out.println("loginSuccessful");
        try{
            client.login(TestConfiguration.NESSUS_USER, 
                    TestConfiguration.NESSUS_PASS);
            if (!client.isAuthenticated()){
                fail("isAuthenticated should return true");
            }
        } catch (SessionException ex){
            fail(ex.getMessage());
        } catch(NessusException ex){
            fail(ex.getMessage());
        }
    }
    
}
