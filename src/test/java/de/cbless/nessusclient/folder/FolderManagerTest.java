/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.nessusclient.folder;

import de.cbless.nessusclient.ManagerFactory;
import de.cbless.nessusclient.NessusClient;
import de.cbless.nessusclient.NessusClientBuilder;
import de.cbless.nessusclient.NessusException;
import de.cbless.nessusclient.SessionException;
import de.cbless.nessusclient.TestConfiguration;
import de.cbless.nessusclient.folders.FolderManager;
import de.cbless.nessusclient.folders.model.resources.Folder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author christoph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FolderManagerTest {
    
    private NessusClient nessusClient;
    private FolderManager folderManager;
    
    
    private static int createdScanId;
    private static final String CREATE_NAME ="CreateFolder";
    private static final String EDIT_NAME ="EditFolder";
    
    public FolderManagerTest() throws NoSuchAlgorithmException, KeyManagementException {
        NessusClientBuilder builder = new NessusClientBuilder(TestConfiguration.NESSUS_URL, true);        
        nessusClient = builder.build();
        try {
            nessusClient.login(TestConfiguration.NESSUS_USER, 
                    TestConfiguration.NESSUS_PASS);
            folderManager = new ManagerFactory(nessusClient).createFolderManager();
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (NessusException ex ){
            fail(ex.getMessage());
        }   
    }
    
    /**
     * Test of create method, of class FolderManager.
     */
    @Test
    public void test1Create() {
        createdScanId = folderManager.create(CREATE_NAME);
        assertNotNull(createdScanId);
    }
        
    @Test 
    public void test2ListAfterCreate(){
        Folder f = folderManager.listById(createdScanId);
        assertNotNull(f);
        assertEquals(createdScanId, f.getId());
        assertEquals(CREATE_NAME, f.getName());
    }
    
    @Test 
    public void test3Edit(){
        folderManager.edit(createdScanId, EDIT_NAME);
    }
    
    @Test 
    public void test4ListByName(){
        List<Folder> folders = folderManager.listByName(EDIT_NAME);
        for (Folder f : folders){
            assertEquals(EDIT_NAME, f.getName());
        }
    }
    
    
    @Test 
    public void test5Delete(){
        folderManager.delete(createdScanId);
    }
    
    @Test 
    public void test6ListAfterDelete(){
        List<Folder> response = folderManager.list();
        assertNotNull(response);
        boolean found = false;
        for (Folder f : response){
            if (f.getId() == createdScanId){
                found = true;
            } 
        }
        assertFalse(found);
    }
}
